import logging
import sys
import argparse
import requests
import time
import threading
from requests.auth import HTTPBasicAuth


def create_logger(logfile, loglevel):
    log = logging.getLogger(__name__)
    log.setLevel(loglevel)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(loglevel)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    log.addHandler(handler)
    return log

def create_argparser():
    parser = argparse.ArgumentParser(description='Bamboo deployment automation tool')
    parser.add_argument("--bamboourl", help='URL to bamboo server', default='http://35.246.148.94')
    parser.add_argument("--bamboouser", help='Bamboo username with permissions', default='bamboo')
    parser.add_argument("--bamboopassword", help='Bamboo user password', default='Bamboo123')
    parser.add_argument("--deploymentproject", help='Deployment project name to work with', default='BillingCenter R0.5')
    parser.add_argument("--sourceenv", help='Deployment project name to work with', default='R0.5 DEV')
    parser.add_argument("--targetenv", help='Deployment project name to work with', default='R0.5 QA' )
    parser.add_argument("--passingrule", help='Choose passing rule to move release to higher environment: default same as on sourcedev', default='lastsuccess')
    parser.add_argument("--loglevel", help='Set loglevel for this script, default INFO', default='InFo')
    parser.add_argument("--useflag", help='flag to decide if take into account the approved/broken statuses', action='store_true')
    parser.add_argument("--approvedonly", help='flag to decide if only approved releases can be deployed', action='store_true')
    parser.add_argument("--onlynewer", help='flag to decide if only newer deployments can be perfored', action='store_true')
    parser.add_argument("--dryrun", help='flag to decide if take into account the approved/broken statuses', action='store_true')
    parser.add_argument("--sourcewindow", help='Add start hour which', default='Test Artifactory Cleaniing')

    args = parser.parse_args()
    return args
args = create_argparser()
log = create_logger('log.txt',args.loglevel.upper())

log.info("Starting automation tool for bamboo deployment")
def call_bamboo_api(type,url,user_name,user_password):
    log.info("performing request to: "+url+" request type "+type)
    payload = ""
    headers = {}
    try:
        response = requests.request(type, url, data=payload, headers=headers, auth=HTTPBasicAuth(user_name, user_password))
        response.raise_for_status()
    except requests.exceptions.Timeout:
        log.warning("timeout occured for url:"+url)
        return [None,-2]
    except requests.exceptions.ConnectionError:
        log.warning("remove server refuesed connection "+url)
        return [None,-1]
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 403:
            log.warning("http 403 error occured")
            return [None,-403]
    return [response,None]

def get_bamboo_envrionments(bamboo_url,bamboo_user,bamboo_pass, deployment_project_name):
    deployment_project_rest = bamboo_url+'/rest/api/latest/deploy/project/all'
    deployment_project_list = call_bamboo_api("GET",deployment_project_rest,bamboo_user,bamboo_pass)
    dp_json = deployment_project_list[0].json()
    dp_dict = NotImplemented
    dp_envs = {}
    for dp in dp_json:
        if dp["name"] == deployment_project_name:
            log.info("The deployment project "+deployment_project_name+" has been found")
            dp_dict=dp
    if dp_dict is NotImplemented:
        log.error("could not find deployment project. Please verify user permission or deployment project name")
        exit(55)

    for env in dp_dict["environments"]:
        log.info("env name: "+env["name"]+ " bamboo internal env id: "+ str(env["id"]))
        dp_envs[env["id"]]=env["name"]
    
    if not bool(dp_envs):
        log.error("the deployment project "+deployment_project_name+" do not have any envs")
        exit(5)
    log.info("method return: "+ str(dp_envs))
    return dp_envs

def get_bamboo_envrionments_codes(bamboo_url,bamboo_user,bamboo_pass, deployment_project_name,source,target):
    deployment_project_rest = bamboo_url+'/rest/api/latest/deploy/project/all'
    deployment_project_list = call_bamboo_api("GET",deployment_project_rest,bamboo_user,bamboo_pass)
    dp_json = deployment_project_list[0].json()
    dp_dict = NotImplemented
    dp_envs = {}
    for dp in dp_json:
        if dp["name"] == deployment_project_name:
            log.info("The deployment project "+deployment_project_name+" has been found")
            dp_dict=dp
    if dp_dict is NotImplemented:
        log.error("could not find deployment project. Please verify user permission or deployment project name")
        exit(55)

    for env in dp_dict["environments"]:
        log.info("env name: "+env["name"]+ " bamboo internal env id: "+ str(env["id"]))
        log.info("checking if environment")
        dp_envs[env["id"]]=env["name"]
    
    if not bool(dp_envs):
        log.error("the deployment project "+deployment_project_name+" do not have any envs")
        exit(5)
    log.info("method return: "+ str(dp_envs))
    return dp_envs


dict_envs = get_bamboo_envrionments(args.bamboourl,args.bamboouser,args.bamboopassword, args.deploymentproject)


def collect_environment_releases(bamboo_url,bamboo_user,bamboo_pass,env_name,dp_envs):
    log.info("checking if environemnt "+env_name+" exists in DP")
    source_data = None
    env_id = ''
    for id,env in dict_envs.items():
        log.info("env "+ str(id)+ " name: "+str(env))
        if env == env_name:
            env_id=str(id)
            url=bamboo_url+'/rest/api/latest/deploy/environment/'+str(id)+'/results'
            deployment_project_list = call_bamboo_api("GET",url,bamboo_user,bamboo_pass)
            if not deployment_project_list[1]:
                releases_json = deployment_project_list[0].json()
                log.info("releases on environment: "+str(releases_json))
                source_data = releases_json
            else:
                log.error("Server do not response"+str(deployment_project_list[1]))

    return [source_data,env_id]

source_d = collect_environment_releases(args.bamboourl,args.bamboouser,args.bamboopassword, args.sourceenv,dict_envs)
source_deployments = source_d[0]
source_env_id=source_d[1]

if source_env_id == '' or source_env_id is None:
    log.error("source env does not exists. check provieded name")
    exit(101)

log.info(" source env id that will be taken "+source_env_id)
target_d = collect_environment_releases(args.bamboourl,args.bamboouser,args.bamboopassword, args.targetenv,dict_envs)
target_deployments = target_d[0]

target_env_id=target_d[1]
if target_env_id == '' or target_env_id is None:
    log.error("target env does not exists. check provieded name")
    exit(101)
log.info(" target env id that will be taken "+target_env_id)


log.info("checking target deployments:"+str(target_deployments["results"]) )
if not target_deployments["results"]:
    log.warning("no deployments on target env, skipping validation")
    target_deploy=["not_deployed_yet",0]
else:
    target_deploy = [str(target_deployments["results"][0]["deploymentVersion"]["id"]),int(target_deployments["results"][0]["deploymentVersion"]["creationDate"])]

source_deploy=''
for release in source_deployments["results"]:
    #log.info(" rlease name: "+ str(release))
    if release["deploymentState"] =="SUCCESS" and release["deploymentVersion"]["creationDate"]>=target_deploy[1]:
        log.info("full relase is: "+str(release["deploymentVersion"]))
        status = release["deploymentVersion"].get("versionStatus",{})
        log.info("status parameter " + str(status.get("versionState","someDiffrentValue")) )
        #log.info(" depoyment version id is:" + str(release["deploymentVersion"]["id"])+" it was started at "+str(release["startedDate"])+" and finieshed at"+str(release["finishedDate"]) + " and build statuss is" +str(release["deploymentVersion"]["versionStatus"][0]["versionState"]) )
        if args.useflag:
            if not status.get("versionState","someDiffrentValue")=='BROKEN':
                log.info(" flag checked depoyment version id is:" + str(release["deploymentVersion"]["id"])+" it was started at "+str(release["startedDate"])+" and finieshed at"+str(release["finishedDate"]) )
                if args.approvedonly:
                    if status.get("versionState","someDiffrentValue")=='APPROVED':
                        source_deploy=str(release["deploymentVersion"]["id"])
                        break
                else:
                        source_deploy=str(release["deploymentVersion"]["id"])
                        break
        else:
            source_deploy=str(release["deploymentVersion"]["id"])
            break


log.info("target deploy version is "+str(target_deploy[0]))

log.info("source deploy version is "+str(source_deploy))



if source_deploy==target_deploy[0]:
    log.warning("source and target has the same release. No deployment scheduled")
    exit(0)
elif source_deploy == '':
    log.warning("there is no release to deploy, newer version probaby on target environment or no approved deploymentif --aprovedonly flag used")
    exit(0)
else:
    log.info("source deployment release will be pushed to target environment")
    api_push_deployment_url=args.bamboourl+"/rest/api/latest/queue/deployment/?environmentId="+target_env_id+"&versionId="+source_deploy
    log.info("api url to use: "+api_push_deployment_url)
    if not args.dryrun:
        call_bamboo_api("POST",api_push_deployment_url,args.bamboouser,args.bamboopassword)
                
