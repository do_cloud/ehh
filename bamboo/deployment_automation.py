import logging
import sys
import argparse
import requests
import time
import threading
from requests.auth import HTTPBasicAuth


def create_logger(logfile, loglevel):
    log = logging.getLogger(__name__)
    log.setLevel(loglevel)
    # create a file handler
    #handler = logging.FileHandler(logfile)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(loglevel)
    # create a logging format
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    log.addHandler(handler)
    return log

def create_argparser():
    parser = argparse.ArgumentParser(description='Bamboo deployment automation tool')
    parser.add_argument("--bamboourl", help='URL to bamboo server', default='http://localhost:8085')
    parser.add_argument("--bamboouser", help='Bamboo username with permissions', default='wojtek')
    parser.add_argument("--bamboopassword", help='Bamboo user password', default='wojtek')
    parser.add_argument("--deploymentproject", help='Deployment project name to work with', default='deploy to dev')
    parser.add_argument("--sourceenv", help='Deployment project name to work with', default='dev')
    parser.add_argument("--targetenv", help='Deployment project name to work with', default='qa')
    parser.add_argument("--passingrule", help='Choose passing rule to move release to higher environment: default same as on sourcedev', default='lastsuccess')
    parser.add_argument("--loglevel", help='Set loglevel for this script, default INFO', default='InFo')
    args = parser.parse_args()
    return args
args = create_argparser()
log = create_logger('log.txt',args.loglevel.upper())

log.info("Starting automation tool for bamboo deployment")
def call_bamboo_api(url,user_name,user_password):
    log.info("performing request to: "+url)
    payload = ""
    headers = {}
    try:
        response = requests.request("GET", url, data=payload, headers=headers, auth=HTTPBasicAuth(user_name, user_password))
        response.raise_for_status()
    except requests.exceptions.Timeout:
        log.warning("timeout occured for url:"+url)
        return ['',-2]
    except requests.exceptions.ConnectionError:
        log.warning("remove server refuesed connection "+url)
        return ['',-1]
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 403:
            log.warning("http 403 error occured")
            return ['',-403]
    return [response,None]

def get_bamboo_envrionments(bamboo_url,bamboo_user,bamboo_pass, deployment_project_name):
    deployment_project_rest = bamboo_url+'/rest/api/latest/deploy/project/all'
    deployment_project_list = call_bamboo_api(deployment_project_rest,bamboo_user,bamboo_pass)
    log.info(deployment_project_list[0])
    dp_json = deployment_project_list[0].json()
    dp_dict = NotImplemented
    dp_envs = {}
    for dp in dp_json:
        if dp["name"] == deployment_project_name:
            log.info("The deployment project "+deployment_project_name+" has been found")
            dp_dict=dp
    if dp_dict is NotImplemented:
        log.error("could not find deployment project. Please verify user permission or deployment project name")
        exit(55)

    for env in dp_dict["environments"]:
        log.info("env name: "+env["name"]+ " bamboo internal env id: "+ str(env["id"]))
        dp_envs[env["id"]]=env["name"]
    
    if not bool(dp_envs):
        log.error("the deployment project "+deployment_project_name+" do not have any envs")
        exit(5)
    log.info("method return: "+ str(dp_envs))
    return dp_envs


dict_envs = get_bamboo_envrionments(args.bamboourl,args.bamboouser,args.bamboopassword, args.deploymentproject)


def collect_environment_releases(bamboo_url,bamboo_user,bamboo_pass,env_name,dp_envs):
    log.info("checking if environemnt "+env_name+" exists in DP")
    source_data = None
    for id,env in dict_envs.items():
        log.info("env "+ str(id)+ " name: "+str(env))
        if env == env_name:
            log.info ("source env id is: "+str(id)) 
            # call bamboo for deployments on environment
            url=bamboo_url+'/rest/api/latest/deploy/environment/'+str(id)+'/results'
            deployment_project_list = call_bamboo_api(url,bamboo_user,bamboo_pass)
            log.info("response: "+str(deployment_project_list))
            if not deployment_project_list[1]:
                releases_json = deployment_project_list[0].json()
                log.info("releases on environment: "+str(releases_json))
                source_data = releases_json
            else:
                log.error("Server do not response"+str(deployment_project_list[1]))

    return source_data

source_deployments = collect_environment_releases(args.bamboourl,args.bamboouser,args.bamboopassword, args.sourceenv,dict_envs)

target_deployments = collect_environment_releases(args.bamboourl,args.bamboouser,args.bamboopassword, args.targetenv,dict_envs)

log.info("response value:" + str(source_deployments))


for release in source_deployments["results"]:
    log.info(" rlease name: "+ str(release))


# def validate_runlevel(app_url,expect_runlevel,timeout_time,sleep_time):
#     is_app_running = False
#     while timeout_time > 0:
#         response=call_gw_application(app_url)
#         if response[1]<0:
#             log.warning("app "+app_url+" is not accesible, waiting and retry after "+ str(sleep_time) +" seconds")
#             time.sleep(10)
#             timeout_time = timeout_time -sleep_time
#             log.info("time left: "+str(timeout_time))
#         else:
#             log.info("validating response"+response[0].text)
#             if response[0].text == str(expect_runlevel):
#                 is_app_running = True
#                 timeout = 0
#     return is_app_running
# 
# log.info("validating PC")
# #pb_process = threading.Thread(target = validate_runlevel, args=[pc_ping_url,2,timeout,sleep_time])
# #pb_process.start()
# pc_work_flag = validate_runlevel(pc_ping_url,2,timeout,sleep_time)
# log.info("end of PC validation")
# log.info("validating BC")
# bc_work_flag = validate_runlevel(bc_ping_url,2,timeout,sleep_time)
# log.info("end of BC validation")
# 
# if  pc_work_flag and bc_work_flag:
#     log.info("both apps are working, build can proceed")
#     log.info("Is PC running?: "+str(pc_work_flag)+" Is BC running?: "+str(bc_work_flag))
#     exit(0)
# else:
#     log.error("apps are not running. Is PC running?: "+str(pc_work_flag)+" Is BC running?: "+str(bc_work_flag))
#     exit(2)